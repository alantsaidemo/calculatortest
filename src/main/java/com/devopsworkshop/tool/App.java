package com.devopsworkshop.tool;

import java.util.Random;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

/**
 * Hello world!
 *
 */
public class App 
{
	private static final Logger slf4jLogger = LoggerFactory.getLogger(App.class);
	
    public static void main( String[] args )
    {
    	ConfigurationManager config = new ConfigurationManager("/application.properties");
    	
    	
    	System.out.println( "0.0.2 Environment:" + config.getAppSettings().get("environment") );
    	
        System.out.println( "please enter a number to check for FizzBuzz?" );
        
        Scanner in = new Scanner(System.in);
        
        FizzBuzzCalculator calc = new FizzBuzzCalculator();
        
        Integer input = Integer.parseInt(in.nextLine());
        String result = calc.getString(input);
        
        System.out.println(result);
        
        MDC.put("correlationId", Integer.toString(new Random().nextInt()));
        MDC.put("input",Integer.toString(input));
    	for (int i = 0; i < input; i++) {
    		slf4jLogger.trace("Hello World!");
    		slf4jLogger.info("Welcome to the HelloWorld example of Logback.");
            slf4jLogger.warn("Dummy warning message.");
    	}
    	
        if (result == "Fizz") {
        	try {
        		Integer error = 1 / 0;
        	}catch (Exception ex) {
        		slf4jLogger.error("error", ex);
        	}
        }
    }
}
