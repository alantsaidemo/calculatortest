package com.devopsworkshop.tool;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Properties;

public class ConfigurationManager {
	
	private Properties prop;
	private Hashtable<String, String> AppSettings;

	public ConfigurationManager(String strConfigPath) {
		this.prop = new Properties();
		this.AppSettings = new Hashtable<String, String>();
		
		try {
			InputStream is = getClass().getResourceAsStream(strConfigPath);
			this.prop.load(is);
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public Hashtable<String, String> getAppSettings() {
		this.AppSettings.clear();
		
		for(String key : this.prop.stringPropertyNames()) {
			this.AppSettings.put(key, this.prop.getProperty(key));
		}
		
		return this.AppSettings;
	}
}