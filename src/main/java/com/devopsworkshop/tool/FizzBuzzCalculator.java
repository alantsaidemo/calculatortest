package com.devopsworkshop.tool;

public class FizzBuzzCalculator {
	public String getString(int value) {
		if(isDivideBy3(value) && isDivideBy5(value)) {
			return "FizzBuzz";
		}else if (isDivideBy3(value)) {
			return "Fizz";
		}else if(isDivideBy5(value)){
			return "Buzz";
		}else {
		return Integer.toString(value);
		}
	}
	
	private boolean isDivideBy3(int value) {
		return value % 3 == 0;
	}
	
	private boolean isDivideBy5(int value) {
		return value % 5 == 0;
	}
}
