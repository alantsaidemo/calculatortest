package com.devopsworkshop.tool;

import junit.framework.TestCase;

public class FizzBuzzCalculatorTest extends TestCase {

	public void testShould_return1_when_given1() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.getString(1);
		
		// Assert
		assertEquals("1", result);;
	}
	
	public void testShould_return2_when_given2() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.getString(2);
		
		// Assert
		assertEquals("2", result);;
	}
	
	public void testShould_returnFizz_when_given3() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.getString(3);
		
		// Assert
		assertEquals("Fizz", result);;
	}

	public void testShould_returnBuzz_when_given5() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.getString(5);
		
		// Assert
		assertEquals("Buzz", result);;
	}
	
	public void testShould_returnFizzBuzz_when_given15() {
		// Arrange
		FizzBuzzCalculator sut = new FizzBuzzCalculator();
		
		// Act
		String result = sut.getString(15);
		
		// Assert
		assertEquals("FizzBuzz", result);;
	}
}
